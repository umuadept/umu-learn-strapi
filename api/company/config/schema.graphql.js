module.exports = {
    definition: ``,
    query: `myCompany: Company`,
    type: {},
    resolver: {
      Query: {
        myCompany:{
        description: 'Return company profile',
        resolver: 'application::company.company.myCompany'
        }
      },
    },
  };