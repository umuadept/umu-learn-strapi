'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/v3.x/concepts/controllers.html#core-controllers)
 * to customize this controller
 */

module.exports = {
    myCompany: async ctx =>{
        var companyId = ctx.state.user.company;
        
        var company = strapi.query('company').findOne({ id: companyId });
        return company;	
	}
};
