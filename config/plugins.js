module.exports = ({ env }) => ({
    email: {
        provider: 'mailgun',
        providerOptions: {
            apiKey: env('MAILGUN_API_KEY'),
            domain: env('MAILGUN_DOMAIN')
        },
        settings: {
          defaultFrom: 'littlegodwizard@gmail.com',
          defaultReplyTo: 'littlegodwizard@gmail.com',
        },
    },
    upload:{
        provider: 'aws-s3',
        providerOptions: {
            accessKeyId: env('AWS_ACCESS_KEY_ID'),
            secretAccessKey: env('AWS_ACCESS_SECRET'),
            region: env('AWS_REGION'),
            params: {
                Bucket: env('AWS_BUCKET')
            }
        }
    }  
  })