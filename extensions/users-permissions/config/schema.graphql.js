module.exports = {
  definition: `
  enum ENUM_GENDER {
    male
    female
  }
  type UsersPermissionsMeCustom {
    id: ID!
    username: String!
    email: String!
    name: String
    role: UsersPermissionsRole
    photo: UploadFile
    gender: ENUM_GENDER
    dateBirth: Date
    phoneNumber: String
  }`,
  query: `meCustom: UsersPermissionsMeCustom`,
  type: {},
  resolver: {
    Query: {
      meCustom:{
      description: 'Return user profile custom',
      resolver: 'plugins::users-permissions.user.me'
      }
    },
  },
};