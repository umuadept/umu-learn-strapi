'use strict';

/**
 * Email.js controller
 *
 * @description: A set of functions called "actions" of the `email` plugin.
 */

module.exports = {
    send: async ctx => {
      let options = ctx.request.body;
      try {
        await strapi.plugins.email.services.email.send({
            to: options.to,
            from: 'adeptformsmailgun@andbox53cd88fee49040caa8b8eb5794d372d9.mailgun.org',
            replyTo: 'littlegodwizard@gmail.com',
            subject: 'Test Email',
            text: 'This is test sending email',
            html: '<h1>This is test sending email</h1>'
         });
  
      } catch (e) {
        if (e.statusCode === 400) {
          return ctx.badRequest(e.message);
        } else {
          throw new Error(`Couldn't send email: ${e.message}.`);
        }
      }
  
      // Send 200 `ok`
      ctx.send({});
    }
}