FROM node:12-slim
WORKDIR /usr/src/app
COPY package*.json ./
RUN npm install --unsafe-perm
COPY . ./
CMD [ "npm", "run", "develop" ]